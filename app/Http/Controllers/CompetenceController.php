<?php

namespace App\Http\Controllers;

use App\Models\Competence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CompetenceController extends Controller
{
    public function index(){
        $competences = Competence::all();
        return view('view_admin.competences.index', compact('competences') );
    }

    public function store(Request $request){
        $competence = [
            'comptence' => $request->comptence,
            'pourcentage'=>$request->pourcentage
        ];

        Competence::create($competence);
        return redirect()->back()->with('message','competence crée avec success.');
    }

    public function update(Request $request, Competence $competence){
        $newCompetence = [
            'comptence' => $request->comptence,
            'pourcentage'=>$request->pourcentage
        ];

        $competence->update($newCompetence);

        return redirect()->back()->with('message','competence mise à jour avec success.');
    }

    public function active( Competence $competence){
        $competence->update(["status"=>!$competence->status]);
        $message = ($competence->status)? 'competence publier !' : 'competence cacher';
        return Redirect()->back()->with('message',$message);
    }

    public function delete(Competence $competence){
        $competence->delete();
        return Redirect()->back()->with('message','competence supprimer avec success');
    }
}
