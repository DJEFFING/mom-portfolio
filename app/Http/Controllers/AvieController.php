<?php

namespace App\Http\Controllers;

use App\Models\Avie;
use Illuminate\Http\Request;

class AvieController extends Controller
{
    public function store(Request $request){
        $request->validate(
            [
                'message_avies'=>'required|min:10'
        ],[
            'message_avies.required'=>'vous devez ajouter un temoignage !',
            'message_avies.min' => 'se champ dois contennir un minimun de 10 carratère'
        ]);
        $avie = [
            'nom'=>$request->nom,
            'email' =>$request->email,
            'message_avies' => $request->message_avies,
            'service_id' => $request->service_id
        ];
        Avie::create($avie);
        return redirect()->back()->with('message','merci de nous avoir donner votre avie !!');
    }
}
