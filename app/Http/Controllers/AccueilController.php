<?php

namespace App\Http\Controllers;

use App\Models\Apropos;
use App\Models\Competence;
use App\Models\Resume;
use App\Models\Service;
use App\Models\Slide;
use Illuminate\Http\Request;

class AccueilController extends Controller
{
    public function index(){
        $resumes = Resume::all();
        $slides = Slide::where('is_active',1)->get();
        $services = Service::all();
        $apropos = Apropos::all()->first();
        $competences = Competence::where('status',1)->get();
        return view('view_site.index',compact('slides','apropos','resumes','services','competences'));
    }
}
