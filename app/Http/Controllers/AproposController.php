<?php

namespace App\Http\Controllers;

use App\Models\Apropos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AproposController extends Controller
{
    public function index(){
        $apropos = Apropos::all();
        return view('view_admin.apropos.index',compact('apropos'));

    }

    public function store(){

    }

    public function update(Request $request, Apropos $apropos){
        $data = $request->except('_token','photo');
        if(isset($request->photo)){
            File::delete('storage/'.$apropos->photo);
            $data['photo'] = $request->photo->store('Apropos','public');
        }
        $apropos->update($data);

        return redirect()->back()->with('message','la section apropos à été mise à jour !!');
    }

    public function delete( Apropos $apropos){
        File::delete('storage/'.$apropos->photo);
        $apropos->delete();
        return redirect()->back()->with('message','la section apropos à été supprimer');

    }
}
