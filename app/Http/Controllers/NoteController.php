<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;

use function Laravel\Prompts\note;

class NoteController extends Controller
{
    public function store(Request $request){
        $note = [
            'note' => $request->note,
            'service_id' => $request->service_id
        ];
        Note::create($note);
        return redirect()->back()->with('message','merci de nous avoir atribuer une note');
    }
}
