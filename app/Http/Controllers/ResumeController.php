<?php

namespace App\Http\Controllers;

use App\Models\Resume;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    public function index(){
        $resumes = Resume::all();
        return view('view_admin.resume.index',compact('resumes'));
    }

    public function store(Request $request){
        $request->validate([
            'annee'=>'required|min:9',
            'classe'=>'required',
            'ecole' => 'required',
            'description' => 'required|min:15',
        ],[
            'annee.required'=>'se champ est obligatoire',
            'classe.required'=>'se champ est obligatoire',
            'ecole.required'=>'se champ est obligatoire',
            'description.required'=>'se champ est obligatoire',
            'annee.min'=>'lse doit avoir un minimun de 9 carratère',
            'description.min'=>'lse doit avoir un minimun de 9 carratère',

        ]);

        $resume =[
            'annee' => $request->annee,
            'classe' => $request->classe,
            'ecole' => $request->ecole,
            'description' => $request->description
        ];

       Resume::create($resume);
        return redirect()->back()->with('message','création du résumé réussie !!');
    }

    public function update(Request $request,Resume $resume){
        $request->validate([
            'annee'=>'required|min:9',
            'classe'=>'required',
            'ecole' => 'required',
            'description' => 'required|min:15',
        ],[
            'annee.required'=>'se champ est obligatoire',
            'classe.required'=>'se champ est obligatoire',
            'ecole.required'=>'se champ est obligatoire',
            'description.required'=>'se champ est obligatoire',
            'annee.min'=>'lse doit avoir un minimun de 9 carratère',
            'description.min'=>'lse doit avoir un minimun de 9 carratère',

        ]);

        $newResume =[
            'annee' => $request->annee,
            'classe' => $request->classe,
            'ecole' => $request->ecole,
            'description' => $request->description
        ];

        $resume->update($newResume);
        return redirect()->back()->with('message','mise ajour du résumé réussie !!');
    }

    public function delete(Resume $resume){
        $resume->delete();
        return redirect()->back()->with('message','supression du résumé reussie !!');
    }
}
