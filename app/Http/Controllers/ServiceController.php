<?php

namespace App\Http\Controllers;

use App\Models\Avie;
use App\Models\Note;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{

    public function index()
    {
        $services = Service::all();
        return view('view_admin.services.index',compact('services'));
    }


    public function create()
    {
        return view('view_admin.services.create');
    }


    public function store(Request $request)
    {
        $media[] = "";
      $data = $request->except('_token','media');
      if($request->hasFile('media')){
        foreach($request->media as $item){
            $media[] = $item->store('service','public');
        }
        $data['media'] = json_encode($media);
      }
      Service::create($data);
      return redirect()->route('admin.service.index')->with('message','service enregistrer avec success !!');
    }


    public function show(Service $service)
    {
        return view('view_admin.services.show',['service'=>$service]);
    }

    public function showSite(Service $service)
    {
        $avies = Avie::where('service_id',$service->id)->get();
        $nbreTemoignage = Avie::where('service_id',$service->id)->count();
        $note = $this->calculNote($service);
        $noteAvie = Note::where('service_id',$service->id)->count();
        return view('view_site.services.serviceDetails',
                    
        [ 'service'=>$service ,
                     'avies'=>$avies,
                        'nbreAvie'=>$nbreTemoignage,
                        'note' => $note,
                        'noteAvie'=>$noteAvie
                    ]);
    }


    public function edit(Service $service)
    {
        return view('view_admin.services.edit',['service'=>$service]);
    }


    public function update(Request $request,Service $service)
    {
        $media[] = "";
      $data = $request->except('_token','media');
      if($request->hasFile('media')){

        foreach(json_decode($service->media) as $item){
            File::delete('storage/'.$item);
        }

        foreach($request->media as $item){

            $media[] = $item->store('service','public');
        }
        $data['media'] = json_encode($media);
      }
      $service->update($data);
      return redirect()->route('admin.service.index')->with('message','service mise à jour avec success !!');
    }


    public function delete(Service $service)
    {
        foreach(json_decode($service->media) as $item){
            File::delete('storage/'.$item);
        }
        $service->delete();
        return redirect()->back()->with('message','le service à été suprimer avec success. ');
    }

    public function calculNote(Service $service){
        $count = Note::where('service_id',$service->id)->count();
        $sum = 0;
        
        if($count!=0){
            $NoteList = Note::where('service_id',$service->id)->get();
            foreach($NoteList as $item){
                $sum = $sum + $item->note;
            }
            $result = $sum/$count;
            return $result;

        }else{
            return 0;
        }
    }
}
