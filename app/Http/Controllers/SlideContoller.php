<?php

namespace App\Http\Controllers;

use App\Models\Slide;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SlideContoller extends Controller
{
    public function index(){
        $slides = Slide::all();
        return view('view_admin.silde.index',compact('slides'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'titre' => 'required|string|max:255',
            'sous_titre' => 'required|string',
            'photo' => 'required',
        ], [
            'titre.required' => 'Le champ "titre" est requis.',
            'titre.max' => 'Le champ "titre" doit avoir maximum :max caractères.',
            'sous_titre.required' => 'Le champ "Sous titre" est requis.',
            'photo.required' => 'Le champ "photo" est requis.',
        ]);
        $data = $request->except('_token','photo');
        if (isset($request->photo)) {
            $data['photo']=$request->photo->store('slide', 'public');
        }
        Slide::create($data);
        return back()->with('message', 'Ajouté avec succès');
    }

    public function update(Request $request, Slide $slide)
    {
        $request->validate([
            'titre' => 'required|string|max:255',
            'sous_titre' => 'required|string',

        ], [
            'titre.required' => 'Le champ "titre" est requis.',
            'titre.max' => 'Le champ "titre" doit avoir maximum :max caractères.',
            'sous_titre.required' => 'Le champ "Sous titre" est requis.',
            
        ]);
        $data = $request->except('_token','photo');
        if (isset($request->photo)) {
            File::delete('storage/'.$slide->photo);
            $data['photo']=$request->photo->store('slide', 'public');
        }
        $slide->update($data);
        return back()->with('message', 'Ajouté avec succès');
    }

    public function active(Slide $slide)
    {

        $slide->update(["is_active"=>!$slide->is_active]);
        return back()->with('message', 'publication effectuée avec succès');

    }

    public function delete(Slide $slide)
    {
        try{
            File::delete('storage/'.$slide->photo);
            $slide->delete();
            return back()->with('message', 'Suppression effectué avec succes');
        }catch (Exception $e){
            return back()->with('errors', 'une erreur c\'est produite');
        }

    }
}
