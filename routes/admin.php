<?php

use App\Http\Controllers\AproposController;
use App\Http\Controllers\CompetenceController;
use App\Http\Controllers\ResumeController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SlideContoller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('view_admin.index');
});

Route::get('/widgets',function(){
    return view('view_admin.widgets');
})->name('admin.widgets');

Route::get('/generalElement',function(){
    return view('view_admin.elements.generalElement');
})->name('admin.generalElement');

Route::get('/mediaGalerie',function(){
    return view('view_admin.elements.mediaGalerie');
})->name('admin.mediaGalerie');

Route::get('/icons',function(){
    return view('view_admin.elements.icons');
})->name('admin.icons');

Route::get('/invoice',function(){
    return view('view_admin.elements.invoice');
})->name('admin.invoice');

Route::get('/email',function(){
    return view('view_admin.apps.email');
})->name('admin.email');

Route::get('/calander',function(){
    return view('view_admin.apps.calander');
})->name('admin.calander');

Route::get('/tables',function(){
    return view('view_admin.tables');
})->name('admin.tables');

Route::get('/contacts',function(){
    return view('view_admin.contact');
})->name('admin.contacts');

Route::get('/profils',function(){
    return view('view_admin.additionalPage.profils');
})->name('admin.profils');

Route::get('/projets',function(){
    return view('view_admin.additionalPage.projets');
})->name('admin.projets');

Route::get('/login',function(){
    return view('view_admin.additionalPage.login');
})->name('admin.login');

Route::get('/404Page',function(){
    return view('view_admin.additionalPage.404Page');
})->name('admin.404Page');

Route::get('/charts',function(){
    return view('view_admin.charts');
})->name('admin.charts');

Route::get('/settings',function(){
    return view('view_admin.settings');
})->name('admin.settings');

Route::name('admin.')->group(function(){
    Route::controller(SlideContoller::class)->prefix('slide')->name('slide.')->group(function(){
        Route::get('/','index')->name('index');
        Route::post('/','store')->name('store');
        Route::post('update/{slide}','update')->name('update');
        Route::post('active/{slide}','active')->name('is_active');
        Route::delete('delete/{slide}','delete')->name('delete');
    });

    Route::controller(AproposController::class)->prefix('apropos')->name('apropos.')->group(function(){
        Route::get('/','index')->name('index');
        Route::post('/','store')->name('store');
        Route::post('update/{apropos}','update')->name('update');
        Route::delete('delete/{apropos}','delete')->name('delete');
    });

    Route::controller(ResumeController::class)->prefix('resume')->name('resume.')->group(function(){
        Route::get('/','index')->name('index');
        Route::post('/','store')->name('store');
        Route::post('update/{resume}','update')->name('update');
        Route::delete('delete/{resume}','delete')->name('delete');
    });

    Route::controller(ServiceController::class)->prefix('service')->name('service.')->group(function(){
        Route::get('/','index')->name('index');
        
        //formulaire
        Route::get('create/','create')->name('create');
        Route::get('edit/{service}','edit')->name('edit');
        Route::get('show/{service}','show')->name('show');

        //function
        Route::post('/','store')->name('store');
        Route::post('update/{service}','update')->name('update');
        Route::delete('delete/{service}','delete')->name('delete');
    });

    Route::controller(CompetenceController::class)->prefix('competence')->name('competence.')->group(function(){
        Route::get('/','index')->name('index');
        Route::post('/','store')->name('store');
        Route::post('update/{competence}','update')->name('update');
        Route::post('active/{competence}','active')->name('active');
        Route::delete('delete/{competence}','delete')->name('delete');
    });
});

