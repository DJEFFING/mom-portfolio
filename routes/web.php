<?php

use App\Http\Controllers\AccueilController;
use App\Http\Controllers\AvieController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AccueilController::class,'index'])->name('acceuil');

Route::get('/detailBlog',function(){
    return view('view_site.blog.detailBlog');
 })->name('blog.detailBlog');

 Route::post('addAvie/',[AvieController::class,'store'])->name('avie');
 Route::post('note/',[NoteController::class,'store'])->name('note');

 Route::get('serviceDetails/{service}',[ServiceController::class,'showSite'])->name('service.serviceDetails');



