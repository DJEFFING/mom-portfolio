<?php

namespace Database\Seeders;

use App\Models\Resume;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ResumeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $resume_1 = [
            'annee' => '2023-2024',
            'classe' => 'licence technologique CDRI',
            'ecole' => 'IUT-FV',
            'description' =>' j \'ai Obtenue une licence technologique en CDRI (Concepteur Dévellopper Réseau Internet) à l\'Institut universitaire des technologie Fosto victor dans la ville de Bandjoun au Cameroun.'
        ];

        $resume_2 = [
            'annee' => '2022-2023',
            'classe' => 'DUT en GL',
            'ecole' => 'IUT-FV',
            'description' =>' j \'ai Obtenue Diplome Universitaire Des Technologie en GL (Genie Logiciel) à l\'Institut universitaire des technologie Fosto victor dans la ville de Bandjoun au Cameroun.'
        ];

        $resume_3 = [
            'annee' => '2021-2023',
            'classe' => 'NiV 1 GI',
            'ecole' => 'IUT-FV',
            'description'=>' Pendant cette année je fesait mais début dans le dommaine de l\'informatique en GI (Genie Informatique) à l\'Institut universitaire des technologie Fosto victor dans la ville de Bandjoun au Cameroun.'
        ];

        $resume_4 = [
            'annee' => '2020-2021',
            'classe' => 'Tle C',
            'ecole' => 'Lycee Bilingue de Zenmenh Cameroun',
            'description' => 'À la fin de cette année scolaire j\'ai obtennue un Baccalorial Scientifique au lycée Bilingue de Zemenh dans la ville de Dschang au cameroun'
        ];

        $resume_5 = [
            'annee' => '2019-2020',
            'classe' => 'Première C',
            'ecole' => 'Lycee Bilingue de Zenmenh Cameroun',
            'description' => 'À la fin de cette année scolaire j\'ai obtennue un Probatoire Scientifique au lycée Bilingue de Zemenh dans la ville de Dschang au cameroun'
        ];

        $resume_6 = [
            'annee' => '2018-2020',
            'classe' => 'Seconde C',
            'ecole' => 'Lycee Bilingue de Zenmenh Cameroun',
            'description' => 'À la fin de cette année scolaire j\'ai obtennue un Probatoire Scientifique au lycée Bilingue de Zemenh dans la ville de Dschang au cameroun'
        ];

        Resume::create($resume_1);
        Resume::create($resume_2);
        Resume::create($resume_3);
        Resume::create($resume_4);
        Resume::create($resume_5);
        Resume::create($resume_6);
    }
}
