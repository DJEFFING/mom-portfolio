<?php

namespace Database\Seeders;

use App\Models\Competence;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CompetenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $java = [
            'comptence'=> 'java 18',
            'pourcentage'=> 60
        ];

        $python = [
            'comptence'=> 'python 3.10',
            'pourcentage'=> 50
        ];

        $php = [
            'comptence'=> 'php 8',
            'pourcentage'=> 55
        ];
        
        $dart = [
            'comptence' =>'dart',
            'pourcentage' => 60
        ];

        $typeScript = [
            'comptence'=> 'typeScript',
            'pourcentage'=> 45
        ];

        $javaScript = [
            'comptence'=> 'javaScript',
            'pourcentage'=> 45
        ];

        $laravel = [
            'comptence'=> 'laravel',
            'pourcentage'=> 65
        ];

        $springBoot = [
            'comptence'=> 'spring boot',
            'pourcentage'=> 60
        ];

        $node = [
            'comptence'=> 'Node Js',
            'pourcentage'=> 45
        ];

        $Angular = [
            'comptence'=> 'Angular Js',
            'pourcentage'=> 55
        ];

        $flutter = [
            'comptence'=> 'flutter',
            'pourcentage'=> 55
        ];

        Competence::create($java);
        Competence::create($python);
        Competence::create($php);
        Competence::create($dart);
        Competence::create($typeScript);
        Competence::create($javaScript);
        Competence::create($laravel);
        Competence::create($springBoot);
        Competence::create($node);
        Competence::create($Angular);
        Competence::create($flutter);


    }
}
