<?php

namespace Database\Seeders;

use App\Models\Apropos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AproposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $apropos = [
            'nom' => 'Azafack Tsafack Jefferson',
            'date_naissance' => '06 fevrier 2002',
            'adresse' => 'bandjoun iut-fv',
            'code_postal' => '126 bp',
            'email' => 'tsafackjefferson2001@gmail,com',
            'phone' => '+237 675869321/ +237 693177980',
            'projet_compelete' =>100,
            'photo'=> 'tsafack.png'
        ];

        Apropos::create($apropos);
    }
}
