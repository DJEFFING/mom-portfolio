

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">Competence <span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="comptence" placeholder=" votre comptence" value="{{ $competence->comptence?? old('comptence') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('comptence', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">Pourcentage :<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="number" id="text-input" name="pourcentage" placeholder=" Poucentage" value="{{ $competence->pourcentage?? old('pourcentage') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('pourcentage', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>


