

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">annee <span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="annee" placeholder="votre annee" value="{{ $resume->annee?? old('annee') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('annee', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">classe<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="classe" placeholder="classe" value="{{ $resume->classe?? old('classe') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('classe', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">ecole<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="ecole" placeholder="ecole" value="{{ $resume->ecole?? old('ecole') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('ecole', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="summernote" class=" form-control-label">description <span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><textarea name="description" id="summernote" rows="9" placeholder="description..." class="form-control">{{$resume->description?? old('description') }}</textarea>
        <small class="help-block form-text" style="color: red">{!! $errors->first('description', '<p class="help-block" style="color:red;">:message</p>') !!}</small>
    </div>
</div>
