<div class="modal fade show" id="show{{ $resume->id }}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg"   role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(28, 187, 232)">
                <h3 class="modal-title" id="largeModalLabel">Detail</h3>
                <button type="button" style="float: right" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body ">
                <center>
                @if(isset($resume->photo))
                    <div  class="col-md-4 " >
                    <img   src="{{ asset('storage/'.$resume->photo) }}" width="200" height="200" >
                    </div>
                @endif
                </center>
                <p class="card-text"  style="display: flex;">  <h4> <strong>annee : </strong> {!! $resume->annee !!}</h4>   </p><hr>
                <p class="card-text"><h4> <strong>classe: </strong> <h6>{!! $resume->classe !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>adresse: </strong> <h6>{!! $resume->adresse !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>ecole: </strong> <h6>{!! $resume->ecole !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>description: </strong> <h6>{!! $resume->description !!}</h6> </h4></p><hr>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" aria-label="Close" class="btn btn-info">Confirmer</button>
            </div>
        </div>
    </div>
</div>
