@extends('view_admin.layouts.app')
@section('content')
    <div class="midde_cont">
        <div class="container-fluid">
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Resume</h2>
                    </div>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <!-- table section -->
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="card-header">
                                <strong class="card-title">Resume</strong>
                                {{-- <strong class="card-title" style="float: right"><button type="button"
                                        class="btn btn-success mb-1" data-toggle="modal" data-target="#addResume">
                                        <i class="fa fa-plus"></i> Ajouter</button></strong> --}}
                            </div>
                        </div>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>annee</th>
                                            <th>classe</th>
                                            <th>ecole</th>
                                            <th>description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($resumes as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{!! $item->annee !!}</td>
                                                <td>{!! $item->classe !!}</td>
                                                <td>{!! $item->ecole !!}</td>
                                                <td>{!! Str::limit($item->description, 20, '...') !!}</td>
                                                <td class="text-center" style="display: flex">
                                                    <button class="btn btn-info mr-1" data-toggle="modal"
                                                        data-target="#show{{ $item->id }}"><i
                                                            class="fa fa-eye"></i></button>
                                                    <button class="btn btn-warning mr-1" data-toggle="modal"
                                                        data-target="#editApropos{{ $item->id }}"><i
                                                            class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-danger mr-1" title="Supprimer"
                                                        data-toggle="modal" data-target="#delete{{ $item->id }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                                </td>
                                                </td>
                                            </tr>
                                            @include('view_admin.resume.edit', ['resume' => $item])
                                            @include('view_admin.resume.show', ['resume' => $item])
                                            @include('view_admin.global-modal.delete-modal', [
                                                'id' => $item->id,
                                                'url' => route('admin.resume.delete', $item->id),
                                            ])
                                        @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Aucun ...</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @include('view_admin.resume.create')
                </div>
            </div>
            <!-- footer -->
            <div class="container-fluid">
                <div class="footer">
                    <p>Copyright © 2018 Designed by html.design. All rights reserved.</p>
                </div>
            </div>
        </div>
    @endsection
