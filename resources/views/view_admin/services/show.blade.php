@extends('view_admin.layouts.app')
@section('content')
    <div class="midde_cont">
        <section class="ftco-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 ftco-animate">
                        <h2 class="mb-3">{{ $service->titre }}</h2>
                        <p>{!! $service->description_1 !!}</p>
                        <p>
                            <img src="{{ asset('storage/'. json_decode($service->media)[1]) }}" alt="" class="img-fluid">
                        </p>
                        <p>{!! $service->description_2 !!}</p>

                        <p>{!! $service->description_3 !!}</p>

                        <p>
                            <img src="{{ asset('storage/'. json_decode($service->media)[2]) }}" alt="" class="img-fluid">
                        </p>
                        <p>{!! $service->description_4 !!}</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('admin.service.index') }}"
                                class="btn btn-warning mb-1"><i class="fa-mail-replay"></i><- Back</a>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
