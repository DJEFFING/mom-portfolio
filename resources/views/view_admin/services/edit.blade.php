@extends('view_admin.layouts.app')
@section('content')
    <div class="midde_cont">
        <div class="full graph_head">
            <div class="card-header">
                <center><strong class="card-title">Mise à Jour d'un Services</strong></center>
            </div>
        </div>
        <form action="{{ route('admin.service.update', $service->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Titre <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre"
                            placeholder="Ajouter un titre" value="{{$service->titre ?? old('titre') }}" class="form-control" required>
                        <small class="help-block form-text" style="color:red;">{!! $errors->first('titre', '<p class="help-block" style="color:red">:message</p>') !!}</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="summernote" class=" form-control-label">description_1 <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-12 col-md-9"><textarea id="div_editor1" name="description_1" id="summernote" rows="9" placeholder="description..." class="form-control" required>{{$service->description_1 ?? old('description_1') }}</textarea>
                        <small class="help-block form-text" style="color: red">{!! $errors->first('description_1', '<p class="help-block" style="color:red;">:message</p>') !!}</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="summernote" class=" form-control-label">description_2 <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-12 col-md-9"><textarea id="div_editor1" name="description_2" id="summernote" rows="9" placeholder="description..." class="form-control">{{$service->description_2 ?? old('description_2') }}</textarea>
                        <small class="help-block form-text" style="color: red">{!! $errors->first('description_2', '<p class="help-block" style="color:red;">:message</p>') !!}</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="summernote" class=" form-control-label">description_3 <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-12 col-md-9"><textarea id="div_editor1" name="description_3" id="summernote" rows="9" placeholder="description..." class="form-control">{{$service->description_3 ?? old('description_3') }}</textarea>
                        <small class="help-block form-text" style="color: red">{!! $errors->first('description_3', '<p class="help-block" style="color:red;">:message</p>') !!}</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="summernote" class=" form-control-label">description_4 <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-12 col-md-9"><textarea id="div_editor1" name="description_4" id="summernote" rows="9" placeholder="description..." class="form-control">{{$service->description_4 ?? old('description_4') }}</textarea>
                        <small class="help-block form-text" style="color: red">{!! $errors->first('description_4', '<p class="help-block" style="color:red;">:message</p>') !!}</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Class Icon <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="code_icon"
                            placeholder="Ajouter un code_icon pour l'acceuil" value="{{$service->code_icon ?? old('code_icon') }}" class="form-control" required>
                        <small class="help-block form-text" style="color:red;">{!! $errors->first('code_icon', '<p class="help-block" style="color:red">:message</p>') !!}</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="file-multiple-input" class=" form-control-label">media<span
                                class="text-danger">*</span></label></div>
                    <div class="col-12 col-md-9"><input type="file" id="file-multiple-input" name="media[]" multiple="" accept="image/*"
                            class="form-control" required>
                        <small class="help-block form-text" style="color: red">{!! $errors->first('media', '<p class="help-block">:message</p>') !!}</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="{{ route('admin.service.index') }}"
                        class="btn btn-warning mb-1"><i class="fa-mail-replay"></i><- Back</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">annuler</button>
                <button type="submit" class="btn btn-success" onclick="loader_charger(0)"><span
                        id="submit-btn0">Confirmer</span><span class="loader" id="loader0"></span></button>
            </div>
        </form>
    </div>
@endsection
