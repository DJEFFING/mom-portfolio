@extends('view_admin.layouts.app')
@section('content')
    <div class="midde_cont">
        <div class="container-fluid">
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Services</h2>
                    </div>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <!-- table section -->
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="card-header">
                                <strong class="card-title">Services</strong>
                                <strong class="card-title" style="float: right"><a
                                        href="{{ route('admin.service.create') }}" class="btn btn-success mb-1"><i
                                            class="fa fa-plus"></i>Ajouter</a></strong>
                            </div>
                        </div>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>titre</th>
                                            <th>description_1</th>
                                            <th>description_2</th>
                                            <th>description_3</th>
                                            <th>description_4</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($services as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->titre }}</td>
                                                <td>{!! Str::limit($item->description_1, 15, '...') !!}</td>
                                                <td>{!! Str::limit($item->description_2, 30, '...') !!}</td>
                                                <td>{!! Str::limit($item->description_3, 30, '...') !!}</td>
                                                <td>{!! Str::limit($item->description_4, 30, '...') !!}</td>
                                                <td class="text-center" style="display: flex">
                                                    <a href="{{ route('admin.service.show', $item->id) }}"
                                                        class="btn btn-info mr-1"><i class="fa fa-eye"></i></a>

                                                    <a href="{{ route('admin.service.edit', $item->id)}}"
                                                        class="btn btn-warning mr-1"><i
                                                            class="fa fa-pencil-square-o"></i></a>

                                                    <button class="btn btn-danger mr-1" title="Supprimer"
                                                        data-toggle="modal" data-target="#delete{{ $item->id }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                                </td>
                                                </td>
                                            </tr>
                                            {{-- @include('view_admin.silde.edit', ['slide' => $item])
                                            @include('view_admin.silde.show', ['slide' => $item])
                                            @include('view_admin.global-modal.active-modal', [
                                                'item' => $item,
                                                'url' => route('admin.slide.is_active', $item->id),
                                            ]) --}}
                                            @include('view_admin.global-modal.delete-modal', [
                                                'id' => $item->id,
                                                'url' => route('admin.service.delete', $item->id),
                                            ])
                                        @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Aucun slide...</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- @include('view_admin.silde.create') --}}
                </div>
            </div>
            <!-- footer -->
            <div class="container-fluid">
                <div class="footer">
                    <p>Copyright © 2018 Designed by html.design. All rights reserved.</p>
                </div>
            </div>
        </div>
    @endsection
