

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">Titre <span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" placeholder="Titre du slide" value="{{ $slide->titre?? old('titre') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('titre', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">Sous titre<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="sous_titre" placeholder="Sous titre du slide" value="{{ $slide->sous_titre?? old('sous_titre') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('sous_titre', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
        <div class="col col-md-3"><label for="file-multiple-input" class=" form-control-label">Photo<span class="text-danger">*</span></label></div>
        <div class="col-12 col-md-9"><input type="file" id="file-multiple-input"   name="photo"  class="form-control">
            <small class="help-block form-text" style="color: red">{!! $errors->first('photo', '<p class="help-block">:message</p>') !!}</small>
        </div>
    </div>

