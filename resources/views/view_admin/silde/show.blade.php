<div class="modal fade show" id="show{{ $slide->id }}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg"   role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(28, 187, 232)">
                <h3 class="modal-title" id="largeModalLabel">Detail  du slide</h3>
                <button type="button" style="float: right" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body ">
                <center>
                @if(isset($slide->photo))
                    <div  class="col-md-4 " >
                    <img   src="{{ asset('storage/'.$slide->photo) }}" width="200" height="200" >
                    </div>
                @endif
                </center>
                <p class="card-text"  style="display: flex;">  <h4> <strong>Titre : </strong> {!! $slide->titre !!}</h4>   </p><hr>
                <p class="card-text"><h4> <strong>Sous titre: </strong> <h6>{!! $slide->sous_titre !!}</h6> </h4></p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" aria-label="Close" class="btn btn-info">Confirmer</button>
            </div>
        </div>
    </div>
</div>
