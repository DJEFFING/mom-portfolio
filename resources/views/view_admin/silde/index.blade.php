@extends('view_admin.layouts.app')
@section('content')
    <div class="midde_cont">
        <div class="container-fluid">
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Slide</h2>
                    </div>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <!-- table section -->
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="card-header">
                                <strong class="card-title">Slides</strong>
                                <strong class="card-title" style="float: right"><button type="button"
                                        class="btn btn-success mb-1" data-toggle="modal" data-target="#addslide">
                                        <i class="fa fa-plus"></i> Ajouter</button></strong>
                            </div>
                        </div>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Titre</th>
                                            <th>Sous titre</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($slides as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{!! Str::limit($item->titre, 15, '...') !!}</td>
                                                <td>{!! Str::limit($item->sous_titre, 30, '...') !!}</td>
                                                <td><span
                                                        class="{{ $item->is_active ? 'badge badge-success' : 'badge badge-primary' }}">{{ !$item->is_active ? 'Caché' : 'Publié' }}</span>
                                                </td>
                                                <td class="text-center" style="display: flex">
                                                    <button class="btn btn-primary mr-1" data-toggle="modal"
                                                        data-target="#active{{ $item->id }}">{{ $item->is_active ? 'cacher' : 'publier' }}</button>
                                                    <button class="btn btn-info mr-1" data-toggle="modal"
                                                        data-target="#show{{ $item->id }}"><i
                                                            class="fa fa-eye"></i>voir</button>
                                                    <button class="btn btn-warning mr-1" data-toggle="modal"
                                                        data-target="#editslide{{ $item->id }}"><i
                                                            class="fa fa-pencil-square-o"></i>modifier</button>
                                                    <button class="btn btn-danger mr-1" title="Supprimer"
                                                        data-toggle="modal" data-target="#delete{{ $item->id }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                                </td>
                                                </td>
                                            </tr>
                                            @include('view_admin.silde.edit', ['slide' => $item])
                                            @include('view_admin.silde.show', ['slide' => $item])
                                            @include('view_admin.global-modal.active-modal', [
                                                'item' => $item,
                                                'url' => route('admin.slide.is_active', $item->id),
                                            ])
                                            @include('view_admin.global-modal.delete-modal', [
                                                'id' => $item->id,
                                                'url' => route('admin.slide.delete', $item->id),
                                            ])
                                        @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Aucun slide...</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @include('view_admin.silde.create')
                </div>
            </div>
            <!-- footer -->
            <div class="container-fluid">
                <div class="footer">
                    <p>Copyright © 2018 Designed by html.design. All rights reserved.</p>
                </div>
            </div>
        </div>
    @endsection
