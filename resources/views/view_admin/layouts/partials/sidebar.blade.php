<nav id="sidebar">
    <div class="sidebar_blog_1">
        <div class="sidebar-header">
            <div class="logo_section">
                <a href="asset_admin/index.html"><img class="logo_icon img-responsive"
                        src="{{asset('asset_admin/images/logo/logo_icon.png')}}" alt="#" /></a>
            </div>
        </div>
        <div class="sidebar_user_info">
            <div class="icon_setting"></div>
            <div class="user_profle_side">
                <div class="user_img"><img class="img-responsive"
                        src="{{asset('asset_admin/images/layout_img/user_img.jpg')}}" alt="#" /></div>
                <div class="user_info">
                    <h6>John David</h6>
                    <p><span class="online_animation"></span> Online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_blog_2">
        <h4>General</h4>
        <ul class="list-unstyled components">

            <li class="active">
                <a href="#dashboard" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                        class="fa fa-dashboard yellow_color"></i> <span>Dashboard</span></a>
                <ul class="collapse list-unstyled" id="dashboard">
                    <li>
                        <a href="/admin">> <span>Default Dashboard</span></a>
                    </li>
                    <li>
                        <a href="/admin">> <span>Dashboard style 2</span></a>
                    </li>
                </ul>
            </li>
            <li class="active">
                <a href="#view_side" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                        class="fa fa-dashboard yellow_color"></i> <span>View Site</span></a>
                <ul class="collapse list-unstyled" id="view_side">
                    <li>
                        <a href="{{route('admin.slide.index')}}">> <span>Side</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.apropos.index')}}">> <span>A Propos</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.resume.index')}}">> <span>Resume</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.service.index')}}">> <span>Service</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.competence.index')}}">> <span>Competence</span></a>
                    </li>
                </ul>
            </li>
            <li><a href="{{route('admin.widgets')}}"><i class="fa fa-clock-o orange_color"></i>
                    <span>Widgets</span></a></li>
            <li>
                <a href="#element" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                        class="fa fa-diamond purple_color"></i> <span>Elements</span></a>
                <ul class="collapse list-unstyled" id="element">
                    <li><a href="{{route('admin.generalElement')}}">> <span>General Elements</span></a></li>
                    <li><a href="{{route('admin.mediaGalerie')}}">> <span>Media Gallery</span></a></li>
                    <li><a href="{{route('admin.icons')}}">> <span>Icons</span></a></li>
                    <li><a href="{{route('admin.invoice')}}">> <span>Invoice</span></a></li>
                </ul>
            </li>
            <li><a href="{{route('admin.tables')}}"><i class="fa fa-table purple_color2"></i>
                    <span>Tables</span></a></li>
            <li>
                <a href="#apps" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                        class="fa fa-object-group blue2_color"></i> <span>Apps</span></a>
                <ul class="collapse list-unstyled" id="apps">
                    <li><a href="{{route('admin.email')}}">> <span>Email</span></a></li>
                    <li><a href="{{route('admin.calander')}}">> <span>Calendar</span></a></li>
                    <li><a href="{{route('admin.mediaGalerie')}}">> <span>Media Gallery</span></a></li>
                </ul>
            </li>

            <li>
                <a href="{{route('admin.contacts')}}">
                    <i class="fa fa-paper-plane red_color"></i> <span>Contact</span></a>
            </li>
            <li class="active">
                <a href="#additional_page" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle"><i class="fa fa-clone yellow_color"></i> <span>Additional
                        Pages</span></a>
                <ul class="collapse list-unstyled" id="additional_page">
                    <li>
                        <a href="{{route('admin.profils')}}">> <span>Profile</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.projets')}}">> <span>Projects</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.login')}}">> <span>Login</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.404Page')}}">> <span>404 Error</span></a>
                    </li>
                </ul>
            </li>
            <li><a href="{{route('admin.charts')}}"><i class="fa fa-bar-chart-o green_color"></i>
                    <span>Charts</span></a></li>
            <li><a href="{{route('admin.settings')}}"><i class="fa fa-cog yellow_color"></i>
                    <span>Settings</span></a></li>
        </ul>
    </div>
</nav>
