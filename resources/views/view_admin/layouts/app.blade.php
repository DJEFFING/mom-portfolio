<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />

    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    {{-- datatables --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Pluto - Responsive Bootstrap Admin Panel Templates</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- site icon -->
    <link rel="icon" href="{{ asset('asset_admin/images/fevicon.png') }}" type="image/png" />
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('asset_admin/css/bootstrap.min.css') }}" />
    <!-- site css -->
    <link rel="stylesheet" href="{{ asset('asset_admin/style.css') }}" />
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('asset_admin/css/responsive.css') }}" />
    <!-- color css -->
    <link rel="stylesheet" href="{{ asset('asset_admin/css/colors.css') }}" />
    <!-- select bootstrap -->
    <link rel="stylesheet" href="{{ asset('asset_admin/css/bootstrap-select.css') }}" />
    <!-- scrollbar css -->
    <link rel="stylesheet" href="{{ asset('asset_admin/css/perfect-scrollbar.css') }}" />
    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('asset_admin/css/custom.css') }}" />

    <link rel="stylesheet" href=" {{ asset('asset_admin/css/perfect-scrollbar.css') }} " />
    <!-- custom css -->
    <link rel="stylesheet" href=" {{ asset('asset_admin/css/custom.css') }} " />
    <!-- calendar file css -->
    <link rel="stylesheet" href=" {{ asset('asset_admin/js/semantic.min.css') }} " />
    <!-- fancy box js -->
    <link rel="stylesheet" href=" {{ asset('asset_admin/css/jquery.fancybox.css') }} " />
    <!-- calendar file css -->
    <link rel="stylesheet" href=" {{ asset('asset_admin/js/semantic.min.css') }} " />

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js "
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css"rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .material-icons {
            color: rgb(243, 243, 50);
        }

        .star {
            font-size: 30px;
            color:rgb(243, 243, 50);
        }
    </style>

</head>

<body
    class="dashboard dashboard_1 profile_page login tables_page contact_page inner_page widgets  general_elements invoice_page media_gallery icons_page invoice_page email_page">
    <div class="full_container">
        <div class="inner_container">

            <!-- Sidebar  -->
            @include('view_admin.layouts.partials.sidebar')
            <!-- end sidebar -->

            <!-- right content -->
            <div id="content">
                <!-- topbar -->
                @include('view_admin.layouts.partials.topBar')
                <!-- end topbar -->
                @if (Session::has('message'))
                    <div class="position-fixed top-5 end-0 p-3" id="masque" style="z-index: 5">
                        <div class="bs-toast toast show bg-success" role="alert" aria-live="assertive"
                            aria-atomic="true" data-delay="2000">
                            <div class="toast-header">
                                <i class="bx bx-bell me-2"></i>
                                <div class="me-auto fw-semibold">Notification</div>
                                <small>1s ago</small>
                                <button type="button" class="btn-close" data-bs-dismiss="toast"
                                    aria-label="Close"></button>
                            </div>
                            <div class="toast-body">
                                {{ Session::get('message') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if (Session::has('error'))
                    <div class="position-fixed top-5 end-0 p-3" style="z-index: 5">
                        <div class="bs-toast toast show bg-danger" role="alert" aria-live="assertive"
                            aria-atomic="true" data-delay="2000">
                            <div class="toast-header">
                                <i class="bx bx-bell me-2"></i>
                                <div class="me-auto fw-semibold">Notification</div>
                                <small>1s ago</small>
                                <button type="button" class="btn-close" data-bs-dismiss="toast"
                                    aria-label="Close"></button>
                            </div>
                            <div class="toast-body">
                                {{ Session::get('error') }}
                            </div>
                        </div>
                    </div>
                @endif

                @yield('content')
            </div>


        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('asset_admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('asset_admin/js/popper.min.js') }}"></script>
    <script src="{{ asset('asset_admin/js/bootstrap.min.js') }}"></script>
    <!-- wow animation -->
    <script src="{{ asset('asset_admin/js/animate.js') }}"></script>
    <!-- select country -->
    <script src="{{ asset('asset_admin/js/bootstrap-select.js') }}"></script>
    <!-- owl carousel -->
    <script src="{{ asset('asset_admin/js/owl.carousel.js') }}"></script>
    <!-- chart js -->
    <script src="{{ asset('asset_admin/js/Chart.min.js') }}"></script>
    <script src="{{ asset('asset_admin/js/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('asset_admin/js/utils.js') }}"></script>
    <script src="{{ asset('asset_admin/js/analyser.js') }}"></script>
    <!-- nice scrollbar -->
    <script src="{{ asset('asset_admin/js/perfect-scrollbar.min.js') }}"></script>
    <script>
        var ps = new PerfectScrollbar('#sidebar');
    </script>
    <!-- custom js -->
    <script src="{{ asset('asset_admin/js/custom.js') }}"></script>
    <script src="{{ asset('asset_admin/js/chart_custom_style1.js') }}"></script>

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('assets/admin/dist/js/glightbox.js') }}"></script>
    <script>
        $('#summernote').summernote({
            placeholder: 'HelloBootstrap4',
            tabsize: 2,
            height: 100
        });
    </script>
    <!-- page script -->
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                scrollX: true,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

        });
    </script>

    <script>
        var editor1 = new RichTextEditor("#div_editor1").summernote({
            tabsize: 5, // Le nombre d'espaces à utiliser pour l'indentation lorsque la touche "Tab" est pressée.
            width: 4 // La largeur de l'éditeur en pixels.
        });
    </script>

    <script>
        var lightbox = GLightbox();
        lightbox.on('open', (target) => {
            console.log('lightbox opened');
        });
        var lightboxDescription = GLightbox({
            selector: '.glightbox2'
        });
        var lightboxVideo = GLightbox({
            selector: '.glightbox3'
        });
        lightboxVideo.on('slide_changed', ({
            prev,
            current
        }) => {
            console.log('Prev slide', prev);
            console.log('Current slide', current);

            const {
                slideIndex,
                slideNode,
                slideConfig,
                player
            } = current;

            if (player) {
                if (!player.ready) {
                    // If player is not ready
                    player.on('ready', (event) => {
                        // Do something when video is ready
                    });
                }

                player.on('play', (event) => {
                    console.log('Started play');
                });

                player.on('volumechange', (event) => {
                    console.log('Volume change');
                });

                player.on('ended', (event) => {
                    console.log('Video ended');
                });
            }
        });

        var lightboxInlineIframe = GLightbox({
            selector: '.glightbox4'
        });
    </script>

    <script>
        const stars = document.querySelectorAll(".star");
        const noteField = document.getElementById("notes");

        stars.forEach(star => {
            star.addEventListener("mouseover", () => {
                const rating = star.getAttribute("data-rating");
                highlightStars(rating);
            });

            star.addEventListener("mouseout", () => {
                const currentRating = noteField.value;
                highlightStars(currentRating);
            });

            star.addEventListener("click", () => {
                const rating = star.getAttribute("data-rating");
                noteField.value = rating;
            });
        });

        function highlightStars(rating) {
            stars.forEach(star => {
                const starRating = star.getAttribute("data-rating");
                if (starRating <= rating) {
                    star.textContent = "★"; // Étoile pleine
                } else {
                    star.textContent = "☆"; // Étoile vide
                }
            });
        }
    </script>
</body>

</html>
