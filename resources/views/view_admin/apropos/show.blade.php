<div class="modal fade show" id="show{{ $apropos->id }}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg"   role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(28, 187, 232)">
                <h3 class="modal-title" id="largeModalLabel">Detail</h3>
                <button type="button" style="float: right" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body ">
                <center>
                @if(isset($apropos->photo))
                    <div  class="col-md-4 " >
                    <img   src="{{ asset('storage/'.$apropos->photo) }}" width="200" height="200" >
                    </div>
                @endif
                </center>
                <p class="card-text"  style="display: flex;">  <h4> <strong>nom : </strong> {!! $apropos->nom !!}</h4>   </p><hr>
                <p class="card-text"><h4> <strong>date_naissance: </strong> <h6>{!! $apropos->date_naissance !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>adresse: </strong> <h6>{!! $apropos->adresse !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>code postal: </strong> <h6>{!! $apropos->code_postal !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>email: </strong> <h6>{!! $apropos->email !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>phone: </strong> <h6>{!! $apropos->phone !!}</h6> </h4></p><hr>
                <p class="card-text"><h4> <strong>projet compelete: </strong> <h6>{!! $apropos->projet_compelete !!}</h6> </h4></p><hr>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" aria-label="Close" class="btn btn-info">Confirmer</button>
            </div>
        </div>
    </div>
</div>
