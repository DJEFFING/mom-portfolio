@extends('view_admin.layouts.app')
@section('content')
    <div class="midde_cont">
        <div class="container-fluid">
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Apropos</h2>
                    </div>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <!-- table section -->
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="card-header">
                                <strong class="card-title">Apropos</strong>
                                {{-- <strong class="card-title" style="float: right"><button type="button"
                                        class="btn btn-success mb-1" data-toggle="modal" data-target="#addslide">
                                        <i class="fa fa-plus"></i> Ajouter</button></strong> --}}
                            </div>
                        </div>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>nom</th>
                                            <th>date_naissance</th>
                                            <th>adresse</th>
                                            <th>code_postal</th>
                                            <th>email</th>
                                            <th>phone</th>
                                            <th>projet compelete</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($apropos as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{!! $item->nom !!}</td>
                                                <td>{!! $item->date_naissance  !!}</td>
                                                <td>{!! $item->adresse  !!}</td>
                                                <td>{!! $item->code_postal  !!}</td>
                                                <td>{!! $item->email  !!}</td>
                                                <td>{!! $item->phone  !!}</td>
                                                <td>{!! $item->projet_compelete  !!}</td>
                                                <td class="text-center" style="display: flex">
                                                    <button class="btn btn-info mr-1" data-toggle="modal"
                                                        data-target="#show{{ $item->id }}"><i
                                                            class="fa fa-eye"></i></button>
                                                    <button class="btn btn-warning mr-1" data-toggle="modal"
                                                        data-target="#editApropos{{ $item->id }}"><i
                                                            class="fa fa-pencil-square-o"></i></button>
                                                </td>
                                                </td>
                                            </tr>
                                            @include('view_admin.apropos.edit', ['apropos' => $item])
                                            @include('view_admin.apropos.show', ['apropos' => $item])
                                        @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Aucun ...</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- @include('view_admin.apropos.edit') --}}
                </div>
            </div>
            <!-- footer -->
            <div class="container-fluid">
                <div class="footer">
                    <p>Copyright © 2018 Designed by html.design. All rights reserved.</p>
                </div>
            </div>
        </div>
    @endsection
