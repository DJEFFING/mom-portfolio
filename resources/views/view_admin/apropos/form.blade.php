

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">Nom <span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="nom" placeholder="votre nom" value="{{ $apropos->nom?? old('nom') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('nom', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">date_naissance<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="date_naissance" placeholder="Sous titre du slide" value="{{ $apropos->date_naissance?? old('date_naissance') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('date_naissance', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">adresse<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="adresse" placeholder="Sous titre du slide" value="{{ $apropos->adresse?? old('adresse') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('adresse', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">code_postal<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="code_postal" placeholder="Sous titre du slide" value="{{ $apropos->code_postal?? old('code_postal') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('code_postal', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">email<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="email" placeholder="Sous titre du slide" value="{{ $apropos->email?? old('email') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('email', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">phone<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="phone" placeholder="Sous titre du slide" value="{{ $apropos->phone?? old('phone') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('phone', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="text-input" class=" form-control-label">projet compelete<span class="text-danger">*</span></label>
    </div>
    <div class="col-12 col-md-9"><input type="text" id="text-input" name="projet_compelete" placeholder="Sous titre du slide" value="{{ $apropos->projet_compelete?? old('projet_compelete') }}"  class="form-control">
        <small class="help-block form-text"  style="color:red;">{!! $errors->first('projet_compelete', '<p class="help-block" style="color:red">:message</p>') !!}</small>
    </div>
</div>
<div class="row form-group">
        <div class="col col-md-3"><label for="file-multiple-input" class=" form-control-label">Photo<span class="text-danger">*</span></label></div>
        <div class="col-12 col-md-9"><input type="file" id="file-multiple-input"   name="photo"  class="form-control">
            <small class="help-block form-text" style="color: red">{!! $errors->first('photo', '<p class="help-block">:message</p>') !!}</small>
        </div>
</div>

