@extends('view_site.layouts.app')
@section('content')
    <!-- header section -->
    <section class="hero-wrap js-fullheight" style="background-image: url('{{ asset('storage/' . json_decode($service->media)[1]) }}');"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
                <div class="col-md-12 ftco-animate pb-5 mb-3 text-center">
                    <h1 class="mb-3 bread">{{ $service->titre }}</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="">Home <i
                                    class="ion-ios-arrow-forward"></i></a></span>
                        <span class="mr-2"><a href="">Service<i
                                    class="ion-ios-arrow-forward"></i></a></span><span>Blog Single <i
                                class="ion-ios-arrow-forward"></i></span>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- end header section -->

    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <h2 class="mb-3">{{ $service->titre }}</h2>
                    <p>{!! $service->description_1 !!}</p>
                    <p>
                        <img src="{{ asset('storage/' . json_decode($service->media)[1]) }}" alt=""
                            class="img-fluid">
                    </p>
                    <p>{!! $service->description_2 !!}</p>

                    <p>{!! $service->description_3 !!}</p>

                    <p>
                        <img src="{{ asset('storage/' . json_decode($service->media)[2]) }}" alt=""
                            class="img-fluid">
                    </p>
                    <p>{!! $service->description_4 !!}</p>



                    <div class="about-author d-flex p-4 bg-dark">

                        <div class="bio mr-5">
                            <img src="{{ asset('asset_site/images/person_1.jpg') }}" alt="Image placeholder"
                                class="img-fluid mb-4">
                        </div>
                        <div class="desc">
                            <h3>George Washington</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem
                                necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente
                                consectetur similique, inventore eos fugit cupiditate numquam !</p>
                        </div>
                    </div>

                    <!-- Avie section -->
                    <div class="pt-5 mt-5">
                        <h3 class="mb-5">{{$nbreAvie}} Avies</h3>

                        <!-- comment liste section -->
                        <ul class="comment-list">
                            @forelse ($avies as $item )
                            <li class="comment">
                                <div class="vcard bio">
                                    <img src="{{ asset('asset_site/images/person_1.jpg') }}" alt="Image placeholder">
                                </div>
                                <div class="comment-body">
                                    <h3>{{$item->nom}}</h3>
                                    <div class="meta">{{$item->updated_at}}</div>
                                    <p>{{$item->message_avies}}</p>
                                    <p><a href="#" class="reply">Reply</a></p>
                                </div>
                            </li>
                            @empty
                            @endforelse

                        </ul>
                        <!-- END comment-list -->

                        <!-- leave Avie section -->
                        <div class="comment-form-wrap pt-5">
                            <h3 class="mb-5">Leave a comment</h3>
                            <form action="{{route('avie')}}" method="POST" class="p-5 bg-dark">
                                @csrf

                                <div class="form-group">
                                    <label for="name">Nom *</label>
                                    <input type="text" class="form-control" id="name" name="nom" required value=" {{old('nom')}}">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email *</label>
                                    <input type="email" class="form-control" id="email" name="email" value=" {{old('email')}}" required >
                                </div>

                                    <input type="text" value="{{$service->id}}" class="form-control" id="email" name="service_id" required hidden>

                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea name="message_avies" id="message"  cols="30" rows="10" class="form-control">{{old('message_avies')}}</textarea>
                                    <small class="help-block form-text" style="color: red">{!! $errors->first('message_avies', '<p class="help-block">:message</p>') !!}</small>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Poster votre Temoignage" class="btn py-3 px-4 btn-primary">
                                </div>

                            </form>
                        </div>
                        <!-- end leave Avie section -->

                    </div>
                    <!-- end Avie section -->

                </div>

                <!-- .col-md-8 -->

                <!-- section de droit -->
                <div class="col-lg-4 sidebar ftco-animate">
                    <!-- Service section -->
                    <div class="sidebar-box ftco-animate">
                        <h3 class="heading-sidebar">Autre Service</h3>
                        <ul class="categories">
                            <li><a href="#">Interior Design <span>(12)</span></a></li>
                            <li><a href="#">Exterior Design <span>(22)</span></a></li>
                            <li><a href="#">Industrial Design <span>(37)</span></a></li>
                            <li><a href="#">Landscape Design <span>(42)</span></a></li>
                        </ul>
                    </div>
                    <!-- end Section section -->

                    <!-- recent projet section-->
                    <div class="sidebar-box ftco-animate">
                        <h3 class="heading-sidebar">QUELQUE-UNES DE NOS REALISATIONS</h3>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4"
                                style="background-image: url({{ asset('asset_site/images/image_1.jpg') }});"></a>
                            <div class="text">
                                <h3 class="heading"><a href="#">Why Lead Generation is Key for Business Growth</a>
                                </h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span> March 12, 2019</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4"
                                style="background-image: url({{ asset('asset_site/images/image_2.jpg') }});"></a>
                            <div class="text">
                                <h3 class="heading"><a href="#">Why Lead Generation is Key for Business Growth</a>
                                </h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span> March 12, 2019</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4"
                                style="background-image: url({{ asset('asset_site/images/image_3.jpg') }});"></a>
                            <div class="text">
                                <h3 class="heading"><a href="#">Why Lead Generation is Key for Business Growth</a>
                                </h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span> March 12, 2019</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- recent projet section-->

                    <!-- note Section-->
                    <div class="sidebar-box ftco-animate">
                        <h3>Services De Notation</h3>
                        <div class="card col">
                            <div class="row">
                                <div class="col-md-8">
                                    @for ($i = 1; $i <= 5; $i++)
                                        @if ($i <= floor($note))
                                            <!-- Étoile pleine -->
                                            <i class="material-icons">star</i>
                                        @elseif ($i > floor($note) && $i <= ceil($note))
                                            <!-- Demi-étoile -->
                                            <i class="material-icons">star_half</i>
                                        @else
                                            <!-- Étoile vide -->
                                            <i class="material-icons">star_border</i>
                                        @endif
                                    @endfor
                                </div>
                                <div class="col-md-4">Note Global: {{$note}}</div>
                            </div>

                            <div>{{$noteAvie}} avies</div>

                            <div class="row">
                                <div class="col-md-7">
                                    <span class="star" data-rating="1">☆</span>
                                    <span class="star" data-rating="2">☆</span>
                                    <span class="star" data-rating="3">☆</span>
                                    <span class="star" data-rating="4">☆</span>
                                    <span class="star" data-rating="5">☆</span>
                                </div>
                                <div class="col-md-5">
                                    <form action="{{route('note')}}" method="POST" id="form">
                                        @csrf
                                        <input type="hidden" name="note" id="notes" value="0" readonly />
                                        <input type="text" value="{{$service->id}}" class="form-control" id="email" name="service_id" required hidden>
                                        <input type="submit" value="notez" class="btn btn-success">

                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- end note Section-->
                </div>

            </div>
        </div>
    </section> <!-- .section -->
@endsection
