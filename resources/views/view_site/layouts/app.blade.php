<!DOCTYPE html>
<html lang="en">

<head>
    <title>Clark - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="{{ asset('asset_site/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_site/css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('asset_site/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_site/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_site/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('asset_site/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('asset_site/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('asset_site/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_site/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_site/css/style.css') }}">


    <style>
        .material-icons {
            color: rgb(243, 243, 50);
        }

        .star {
            font-size: 30px;
            color:rgb(243, 243, 50);
        }
    </style>
</head>

<body class="">
    <!-- nav bar -->
    @include('view_site.layouts.partials.nav')
    <!-- end nav bar-->
    @if (Session::has('message'))
        <div class="position-fixed top-5 end-0 p-3" id="masque" style="z-index: 5">
            <div class="bs-toast toast show bg-success" role="alert" aria-live="assertive" aria-atomic="true"
                data-delay="2000">
                <div class="toast-header">
                    <i class="bx bx-bell me-2"></i>
                    <div class="me-auto fw-semibold">Notification</div>
                    <small>1s ago</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('error'))
        <div class="position-fixed top-5 end-0 p-3" style="z-index: 5">
            <div class="bs-toast toast show bg-danger" role="alert" aria-live="assertive" aria-atomic="true"
                data-delay="2000">
                <div class="toast-header">
                    <i class="bx bx-bell me-2"></i>
                    <div class="me-auto fw-semibold">Notification</div>
                    <small>1s ago</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>
    @endif

    @yield('content')

    <!-- footer -->
    @include('view_site.layouts.partials.footer')
    <!-- end footer-->

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4"
                stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4"
                stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>


    <script src="{{ asset('asset_site/js/jquery.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/popper.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('asset_site/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/aos.js') }}"></script>
    <script src="{{ asset('asset_site/js/jquery.animateNumber.min.js') }}"></script>
    <script src="{{ asset('asset_site/js/scrollax.min.js') }}"></script>

    <script src="{{ asset('asset_site/js/main.js') }}"></script>

    <script>
        const stars = document.querySelectorAll(".star");
        const noteField = document.getElementById("notes");

        stars.forEach(star => {
            star.addEventListener("mouseover", () => {
                const rating = star.getAttribute("data-rating");
                highlightStars(rating);
            });

            star.addEventListener("mouseout", () => {
                const currentRating = noteField.value;
                highlightStars(currentRating);
            });

            star.addEventListener("click", () => {
                const rating = star.getAttribute("data-rating");
                noteField.value = rating;
            });
        });

        function highlightStars(rating) {
            stars.forEach(star => {
                const starRating = star.getAttribute("data-rating");
                if (starRating <= rating) {
                    star.textContent = "★"; // Étoile pleine
                } else {
                    star.textContent = "☆"; // Étoile vide
                }
            });
        }
    </script>

</body>

</html>
